Product Plan

Before making a game, we need to have a product plan to know what elements are going to be included in the game which will help the group in test design and implementation. Some of the important element that need to be included in this game are: <br>
•	Game design <br>
•	Database System <br>
•	Score system <br>
•	Chat system <br>
•	Graphical user interface <br>
•	Puzzle system <br>

Game Design

Designing a game is most important and part which will be built first because without designing we can’t produce a game. designing a game will give the development team a clear idea about what to develop and what needs to be included. 

Database System

Database system plays a vital role while designing and testing game or software because after the game is launched the testing team should make sure that all the data is stored properly without any problem. Database system should be secured because it will have all the vital information about the game and the user such as student username, password and game scores. 

Puzzle system

Puzzle system is also most important part which should be tested very carefully and will be built after database system because to store game data. There should not be any fault and bugs in this part because the user will be using this system to play the game and if there occurs some mistake then the student might not get appropriate score.

Score system

After playing the puzzle game students will be given their score to show them how much they have achieved in the game that’s why this is built after score system. This will be based on the time; question attempts and difficulty of the question and testing this should not take long. 

Chat system

Chat system should be built after developing the score system and puzzle system because the main function of game should be working properly before adding more function to the game and testing this should be easy and short.

Graphical user interface

This is also one of the important parts because if the students experience nice graphical user interface then they will be excited to play the history puzzle game without getting bored and smoothly. This should be built at last because having graphical interface with complete functionality of the game will make it easy to run and effectively.
