Test Process and plans 

4.1 Test process 

Testing is carried out within the game so the group can identify any errors or 
bugs beforehand, so the issues can be resolved as the games being developed. 
Test process involves the following: 

Planning 

This means producing a checklist of the overall approach to developing the game 
and the test objectives as well as state all assumptions being made. The plan 
should also specify all the requirements and resources needed for the game so 
this plan can be looked at when checking if the game has met the criteria. 

Analysis and design 

Once the plan is made, it can be used to design the test cases using the 
techniques from the plan. Therefore, design the test conditions, test cases and 
the test environment set-up. 

Implementation and execution  

This involves executing all the test cases on the computer system with an 
automated test tool or manually and the most significant test cases are executed
first. If during execution a test fails it is executed again to make sure the 
errors are recorded, which are then compared with the expected results to try 
understanding the issue occurring. 

Evaluation exit criteria and reporting 

This is the process that suggests when it’s the time to stop testing as many
test cases may fail or cannot be tested anymore. 

Test closure activities 
These activities are conducted when the software is ready to be delivered as all
the errors have been resolved and all the issues have been looked over, the game
is ready to go. 

4.2 Component testing    

This is the type of testing done by the testers in the team to test all the 
components of the software, so the development team knows the component
interface for the game is according to the requirements. The two types of 
components testing are: component testing in small (CTIS) which is when the 
testing done in isolation to other components and component testing in large 
(CTIL), when the testing's done without isolation due to the functionality flow 
of the game. Therefore, parallel to a game the game screen or a login page can 
be seen as a component, where this test is testing the functionality of the 
different pages of the game.         

4.3 System testing   

The purpose of this test is to evaluate the end-to-end system requirements. 
In this case, the game would be tested to make sure all specifications are met 
to satisfy the client and there's no errors in the system or the game and the 
game runs smoothly with no hesitations. The system testing process will be 
complete if the system meets the requirements and the game is completely 
functionable. 

4.4 Acceptance testing 

The final stage is the acceptance testing, this is where our clients, in this 
case the university teachers and students will test the game and tell us if 
they're satisfied or if they have come across any faults in the system to 
overall reject or accept the game. This is also known as the user acceptance 
testing (UAT) and it’s the most crucial procedure for the development of the 
game before it is released into the market, as it tells us if the product is 
successful or not and if it’s ready to be launched. 

Reference:

http://tryqa.com/what-is-fundamental-test-process-in-software-testing/ 