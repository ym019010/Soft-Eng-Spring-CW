### Introduction

Students from Universities will be able to log in to the system; a web-browser graphical-adventure game (all served in HTML format), to read and comprehend various historical sources to score points, which will then be added to a high score table, where high scores will be available through an API method called ‘getHighscores()’.<br>

If a student was struggling with a puzzle in the game or they would like to collaborate on a level, they would be able to access an online forum and an online chat. Both of which will be moderated with an admin, which will also be able to add and/or remove users.

The following is a quality plan for the testing of the above proposal. Below will outline the product plan, quality goals, processes and plans as well as test cases.
