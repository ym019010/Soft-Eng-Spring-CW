# Module Code: CS1SE16
# Assignment report Title: Spring Term-Test    
# Student Number (e.g. 25098635): 28019010 , 28018989, 28016965
# Date (when the work completed): 19/03/2020
# Actual hrs spent for the assignment: 5 hours each totalling 25 hours
# Assignment evaluation (3 key points): 
## Introduction
Students from Universities will be able to log in to the system; a web-browser graphical-adventure game (all served in HTML format), to read and comprehend various historical sources to score points, which will then be added to a high score table, where high scores will be available through an API method called ‘getHighscores()’.<br>

If a student was struggling with a puzzle in the game or they would like to collaborate on a level, they would be able to access an online forum and an online chat. Both of which will be moderated with an admin, which will also be able to add and/or remove users.

The following is a quality plan for the testing of the above proposal. Below will outline the product plan, quality goals, processes and plans as well as test cases.

## Product Plan
Before making a game, we need to have a product plan to know what elements are going to be included in the game which will help the group in test design and implementation. Some of the important element that need to be included in this game are: <br>
•	Game design <br>
•	Database System <br>
•	Score system <br>
•	Chat system <br>
•	Graphical user interface <br>
•	Puzzle system <br>

### Game Design

Designing a game is most important and part which will be built first because without designing we can’t produce a game. designing a game will give the development team a clear idea about what to develop and what needs to be included. 

### Database System

Database system plays a vital role while designing and testing game or software because after the game is launched the testing team should make sure that all the data is stored properly without any problem. Database system should be secured because it will have all the vital information about the game and the user such as student username, password and game scores. 

### Puzzle system

Puzzle system is also most important part which should be tested very carefully and will be built after database system because to store game data. There should not be any fault and bugs in this part because the user will be using this system to play the game and if there occurs some mistake then the student might not get appropriate score.

### Score system

After playing the puzzle game students will be given their score to show them how much they have achieved in the game that’s why this is built after score system. This will be based on the time; question attempts and difficulty of the question and testing this should not take long. 

### Chat system

Chat system should be built after developing the score system and puzzle system because the main function of game should be working properly before adding more function to the game and testing this should be easy and short.

### Graphical user interface

This is also one of the important parts because if the students experience nice graphical user interface then they will be excited to play the history puzzle game without getting bored and smoothly. This should be built at last because having graphical interface with complete functionality of the game will make it easy to run and effectively.

## Quality Goals 
## Test Process and Plans
### Test Process
Testing is carried out within the game so the group can identify any errors or 
bugs beforehand, so the issues can be resolved as the games being developed. 
Test process involves the following: 

Planning 

This means producing a checklist of the overall approach to developing the game 
and the test objectives as well as state all assumptions being made. The plan 
should also specify all the requirements and resources needed for the game so 
this plan can be looked at when checking if the game has met the criteria. 

Analysis and design 

Once the plan is made, it can be used to design the test cases using the 
techniques from the plan. Therefore, design the test conditions, test cases and 
the test environment set-up. 

Implementation and execution  

This involves executing all the test cases on the computer system with an 
automated test tool or manually and the most significant test cases are executed
first. If during execution a test fails it is executed again to make sure the 
errors are recorded, which are then compared with the expected results to try 
understanding the issue occurring. 

Evaluation exit criteria and reporting 

This is the process that suggests when it’s the time to stop testing as many
test cases may fail or cannot be tested anymore. 

Test closure activities 
These activities are conducted when the software is ready to be delivered as all
the errors have been resolved and all the issues have been looked over, the game
is ready to go. 

http://tryqa.com/what-is-fundamental-test-process-in-software-testing/
### Compomemt testing
This is the type of testing done by the testers in the team to test all the 
components of the software, so the development team knows the component
interface for the game is according to the requirements. The two types of 
components testing are: component testing in small (CTIS) which is when the 
testing done in isolation to other components and component testing in large 
(CTIL), when the testing's done without isolation due to the functionality flow 
of the game. Therefore, parallel to a game the game screen or a login page can 
be seen as a component, where this test is testing the functionality of the 
different pages of the game.         

### System testing
The purpose of this test is to evaluate the end-to-end system requirements. 
In this case, the game would be tested to make sure all specifications are met 
to satisfy the client and there's no errors in the system or the game and the 
game runs smoothly with no hesitations. The system testing process will be 
complete if the system meets the requirements and the game is completely 
functionable. 

### Acceptance testing 
The final stage is the acceptance testing, this is where our clients, in this 
case the university teachers and students will test the game and tell us if 
they're satisfied or if they have come across any faults in the system to 
overall reject or accept the game. This is also known as the user acceptance 
testing (UAT) and it’s the most crucial procedure for the development of the 
game before it is released into the market, as it tells us if the product is 
successful or not and if it’s ready to be launched. 

## Test Cases
### Test Case 1 
_Feature_: a user is on the game's login page <br>
<br>
_Background_ : <br>
    _Given_ a student is on the log in page

_Scenario_: A successful log in attempt <br>

   _When_ the student enters the username "< username >", and the password "< password >" <br>
   _Then_ proceed to the game's main menu <p> 
    |  username  |      password       | <br>
    | Student ID | University Password |
    
_Scenario_: An unsuccessful log in attempt <br> 

   _When_ the student enters an incorrect username and/or an incorrect password <br>
   _Then_ the student sees the message "incorrect username or password please try again" <br>
    

### Test case 2
_Feature_: A student is playing the game. <br>

_Background_: 
_Given_ the student is reaching the final stages of a level. <br>

_Scenario_: The student completes the level <br>

_When_ the student achieves a new high score. <br>
_Then_ add the score along with the name to the leaderboard and progress to the next level. 
    
_Scenario_: The student completes the level. <br>

_When_ the student doesn't achieve a new high score. <br>
_Then_ progress to the next level without adding the score to the leaderboard.
    
### Test Case 3
_Feature_: Students are communicating <br>

_Background_: <br>
    _Given_ a word filter is an aspect of the chat feature <br>
    
_Scenario_: Students are talking <br>

_When_ the restricted word is detected by the filter <br>
_Then_ asterisk the characters of the word <br>
_And_ warn the student that swear words are not tolerated 


