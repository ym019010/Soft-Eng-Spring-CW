Feature: A student is playing the game. <br>

Background: 
Given the student is reaching the final stages of a level. <br>

Scenario: The student completes the level. <br>
    When the student achieves a new high score. <br>
    Then add the score along with the name to the leaderboard and progress to the next level. 
    
Scenario: The student completes the level. <br>
    When the student doesn't achieve a new high score. <br>
    Then progress to the next level without adding the score to the leaderboard.
    

