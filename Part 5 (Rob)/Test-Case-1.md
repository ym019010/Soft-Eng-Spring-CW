## Test case 1
Feature: a user is on the game's login page <br>
<br>
Background : <br>
    Given a student is on the log in page

Scenario: A successful log in attempt <br> <br>
   When the student enters the username "< username >", and the password "< password >" <br>
   Then proceed to the game's main menu <p> 
    |  username  |      password       | <br>
    | Student ID | University Password |
    
Scenario: An unsuccessful log in attempt <br> <br> 
   When the student enters an incorrect username and/or an incorrect password <br>
   Then the student sees the message "incorrect username or password please try again" <br>
    